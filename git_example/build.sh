#!/bin/bash

echo Демонстрация работы с git локально
echo
echo Предполагается, что имя пользователя сконфигурировано командой
echo git config --global user.name \"Your name\"
echo Электронная почта сконифугирована командой 
echo git config --global user.email \"you@example.com\"
echo И выполнена команда инициализации \"git init\"
echo
echo Создаем файл main.cpp c помощью команды \'cat\'
[ -f ./main.cpp ] && rm main.cpp
cat << 'EOF' > main.cpp
#include <iostream>

int main()
{
  std::cout << "Hello, World" << std::endl;

  return 0;
}
EOF
echo Вот этот файл:
cat main.cpp
echo
echo Компилируем
g++ -o main main.cpp
echo Запускаем
./main
echo
echo Добавляем в репозиторий 
git add main.cpp
echo Делаем коммит с комментарием в строке \(ключ -m\)
git commit -m "Первая версия"
echo
echo Заменяем \"World\" на \"git\" c помощью строчного редактора \'sed\'
sed -i.bak 's/World/git/g' main.cpp
echo 
echo Вот измененный файл:
cat main.cpp
echo Компилируем
g++ -o main main.cpp
echo Запускаем и видим \'git\' вместо \'World\'
./main
echo
echo Проверяем состояние с помошью \'git status\'
git status
echo 
echo Смотрим разницу с помощью команды \'git diff\'
git diff
echo
echo Делаем коммит изменения
git add main.cpp
git commit -m "Вторая версия"
echo 
echo Смотрим историю изменений с помощью команды \'git log\'
git log
echo Видим два коммита
echo Хотим вернуться к начальной версии, то есть предыдущему коммиту
echo Делаем это с помошью команды  \'git checkout HEAD^\'
git checkout HEAD^
echo
echo Проверяем файл
cat main.cpp
echo Компилируем
g++ -o main main.cpp
echo Запускаем и видим \'World\' вместо \'git\'
./main
echo Возвращаемся к последнему коммиту с помощью \'git checkout master\'
git checkout master
echo
echo Проверяем файл
cat main.cpp
echo Компилируем
g++ -o main main.cpp
echo Запускаем и видим \'git\' вместо \'World\'
./main

