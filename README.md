Первым делом следует прочесть [инструкцию из данного проекта](https://gitlab.com/alexeit/cpp_practice/tree/master/user_manual_gitlab_environment) и выполнить все указанные в ней действия.


Войти в каждый каталог и выполнить ./build.sh

ВНИМАНИЕ:
Для работы с git_example скопировать этот каталог ВНЕ данного дерева каталогов, сделать в этой копии команду 'git init' и запустить <code>./build.sh</code>


# Путеводитель по материалам практики

* [Форматирование вывода с использование setw(), локали и кириллицы в Unicode](https://gitlab.com/alexeit/cpp_practice/tree/master/wide_unicode_io_locale)