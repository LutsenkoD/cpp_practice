// Форматирование вывода с использование setw(), локали и кириллицы в Unicode

#include <iostream>
#include <iomanip> // setw
#include <locale>

using namespace std;

int main()
{
	wcout.imbue(locale("ru_RU.UTF-8")); // заменяем локаль вывода Unicode на русскую
	wcout << setw(10) << "City" << setw(16) << " Population"<< endl; // использовать теперь только wide
	wcout << setw(10) << L"Город" << setw(16) << L"Население" << endl; // L"" для Unicode
	return 0;
}
